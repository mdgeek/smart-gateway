# smart-gateway

Coordinates interactions between the EHR, authorization server, and FHIR server in environments that are not SMART-compliant.

The SMART Gateway consists of a server and a client (browser) component:

## Server Component
The server component is a Spring Boot application that acts as a proxy, intercepting requests from the SMART app
to the FHIR and authorization endpoints.  The proxy augments the requests and/or the responses to make them SMART-
compliant.

### Running the server component
The server component may be started from the command line:
<p>
`java -jar ./smart-gateway.jar`
<p>
The server component will look for a property file called *application.properties* that contains configuration
information such as the FHIR and OAuth2 endpoints and registered SMART apps.  A sample file is included in this project.

## Client Component
The client component provides a SMART container which can host any SMART-compliant application.  It is launched by the
EHR using a URL that includes the SMART app to be loaded and the current clinical context.  It is responsible for
generating a launch token that will be used in the authorization exchange.  When a CCOW context manager is detected,
it will register as a context participant to synchronize clinical context with the EHR (this capability has not been
tested at this time).  The CCOW logic provided is minimal, relying on background polling to detect a context change.
An alternative CCOW web client may be substituted for production use.

### Installing the client component
The client component consists of a launch page (HTML) and a launch script (JavaScript).  Both are packaged within the 
gateway's jar file and require no special setup.  To initiate the launch of a SMART app, the EHR loads the launch
page by providing its URL along with query parameters that indicate the SMART app to run and any clinical context
to be passed.  For example:
<p>
`http://localhost:9999/smart/index.html?target=mysmartapp&patient=%DFN&user=%DUZ`
<p>

