/*
 * #%L
 * SMART Gateway
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * #L%
 */
package edu.utah.kmm.smartgateway;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

@RestController
@SpringBootApplication
public class SmartGatewayApplication {

    private static final Log log = LogFactory.getLog(SmartGatewayApplication.class);

    private static final String OAUTH_EXTENSION_URL = "http://fhir-registry.smarthealthit.org/StructureDefinition/oauth-uris";

    private static final String OAUTH_EXTENSION_JSON = "[{\"url\": \"authorize\",\"valueUri\": \"%1$s/authorize\"},{\"url\": \"token\",\"valueUri\": \"%1$s/token\"}]";

    private final ObjectMapper mapper = new ObjectMapper();

    private final Map<String, Map<String, String>> launchContexts = new ConcurrentHashMap<>();

    @Value("#{${smart.apps}}")
    private final Map<String, String> smartApps = Collections.emptyMap();

    @Value("#{${context.mappings}}")
    private final Map<String, String> contextMappings = Collections.emptyMap();

    @Value("${fhir.endpoint}")
    private String fhirEndpoint;

    @Value("${fhir.metadata.retries:5}")
    private int fhirMetadataRetries;

    @Value("${oauth.authorize.endpoint:#{null}}")
    private String authorizeEndpoint;

    @Value("${oauth.token.endpoint:#{null}}")
    private String tokenEndpoint;

    @Value("${server.base}")
    private String serverBase;

    @Autowired
    private WebClient client;

    private Mono<ResponseEntity<byte[]>> conformance;

    public static void main(String[] args) {
        SpringApplication.run(SmartGatewayApplication.class, args);
    }

    /**
     * Retrieves the FHIR server's conformance statement and adds the SMART extensions for OAuth endpoints.
     * The response is cached.  Will retry every 10 seconds up to the maximum number of times specified by
     * the 'fhir.metadata.retries' property.
     */
    @PostConstruct
    public void init() {
        debug(() -> "Retrieving FHIR conformance data.");
        conformance = new ProxyHelper<byte[]>(client, byte[].class)
                .setPath("/metadata")
                .setHeader(HttpHeaders.ACCEPT, "application/json", "application/fhir+json")
                .setHeader(HttpHeaders.ACCEPT_ENCODING, "*")
                .get(fhirEndpoint)
                .retryWhen(Retry.fixedDelay(fhirMetadataRetries, Duration.ofSeconds(10)))
                .map(this::processConformanceResponse)
                .cache();
        conformance.subscribe();
    }

    private ResponseEntity<byte[]> processConformanceResponse(ResponseEntity<byte[]> resp) {
        debug(() -> "Processing conformance statement...");

        try {
            byte[] body = resp.getBody();
            ObjectNode root = (ObjectNode) mapper.readTree(body);
            ObjectNode rest = (ObjectNode) root.at("/rest/0");
            ObjectNode security = (ObjectNode) rest.get("security");

            if (security == null) {
                security = mapper.createObjectNode();
                rest.set("security", security);
            }

            ArrayNode extArray = (ArrayNode) security.get("extension");

            if (extArray == null) {
                extArray = mapper.createArrayNode();
                security.set("extension", extArray);
            }

            Iterator<JsonNode> iter = extArray.elements();
            ObjectNode extElement = null;

            while (iter.hasNext()) {
                ObjectNode ele = (ObjectNode) iter.next();

                if (OAUTH_EXTENSION_URL.equals(ele.get("url").asText())) {
                    extElement = ele;
                    break;
                }
            }

            if (extElement == null) {
                extElement = extArray.addObject();
                extElement.put("url", OAUTH_EXTENSION_URL);
            } else if (authorizeEndpoint == null || tokenEndpoint == null) {
                ArrayNode exts = (ArrayNode) extElement.get("extension");
                iter = exts == null ? Collections.emptyIterator() : exts.elements();

                while (iter.hasNext()) {
                    ObjectNode ele = (ObjectNode) iter.next();
                    String url = ele.get("url").asText();

                    if (authorizeEndpoint == null && "authorize".equals(url)) {
                        authorizeEndpoint = ele.get("valueUri").asText();
                    } else if (tokenEndpoint == null && "token".equals(url)) {
                        tokenEndpoint = ele.get("valueUri").asText();
                    }
                }
            }

            validateOAuthEndpoints();
            JsonNode oauthExtension = mapper.readTree(String.format(OAUTH_EXTENSION_JSON, serverBase));
            extElement.set("extension", oauthExtension);
            log.info("Completed processing conformance statement.");
            return new ResponseEntity<>(mapper.writeValueAsBytes(root), resp.getHeaders(), resp.getStatusCode());
        } catch (Exception e) {
            log.error("Error processing conformance statement.", e);
            throw new RuntimeException(e);
        }
    }

    private void validateOAuthEndpoints() {
        log.info("Actual FHIR endpoint: " + fhirEndpoint);
        Assert.notNull(authorizeEndpoint, "No authorization endpoint was specified.");
        log.info("Actual authorization endpoint: " + authorizeEndpoint);
        Assert.notNull(tokenEndpoint, "No token endpoint was specified.");
        log.info("Actual token endpoint: " + tokenEndpoint);
    }

    /**
     * Returns The launch url associated with the specified target identifier.
     *
     * @param target The target identifier.
     * @return The target url.
     */
    @GetMapping("/target")
    public String target(@RequestParam String target) {
        debug(() -> "Resolving target: " + target);
        String uri = smartApps.get(target);
        _assert(uri != null, HttpStatus.BAD_REQUEST, () -> "The requested target could not be found: " + target);
        return uri;
    }

    /**
     * Returns a launch token that is associated with a launch context.
     *
     * @param launchContext The launch context.
     * @return A unique launch token.
     */
    @GetMapping("/launch")
    public String launch(@RequestParam Map<String, String> launchContext) {
        String launchId = UUID.randomUUID().toString();
        debug(() -> "Generated launch id: " + launchId);
        launchContexts.put("L:" + launchId, mapContext(launchContext));
        return launchId;
    }

    /**
     * Proxy for an authorization request.  Removes the launch token from the request and re-associates it with the
     * authorization code returned in the response.
     *
     * @param proxy The proxy helper.
     * @return The response.
     */
    @GetMapping("/authorize")
    public Mono<ResponseEntity<byte[]>> authorize(ProxyHelper<byte[]> proxy) {
        debug(() -> "Servicing authorize request.");
        String launch = CollectionUtils.firstElement(proxy.getQueryParams().remove("launch"));
        return proxy.trimPath("/authorize").get(authorizeEndpoint).map(resp -> processAuthorizeResponse(resp, launch));
    }

    private ResponseEntity<byte[]> processAuthorizeResponse(
            ResponseEntity<byte[]> resp,
            String launch) {
        Map<String, String> context = launchContexts.remove("L:" + launch);
        String code = UriComponentsBuilder.fromUri(resp.getHeaders().getLocation()).build().getQueryParams().getFirst("code");
        launchContexts.put("C:" + code, context);
        return resp;
    }

    /**
     * Proxy for the access token request.
     *
     * @param proxy The proxy helper.
     * @return Adds the launch context to the response body.
     */
    @PostMapping(
            path = "/token",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Map>> token(ProxyHelper<Map> proxy) {
        return proxy.getBodyAsFormData().flatMap(data -> {
            String code = data.getFirst("code");
            debug(() -> "Servicing token request for code: " + code);
            return proxy
                    .trimPath("/token")
                    .post(tokenEndpoint)
                    .doOnSuccess(entity -> entity.getBody().putAll(launchContexts.remove("C:" + code)));
        });
    }

    /**
     * Proxy for the metadata request.  Returns the cached FHIR conformance statement with the OAuth endpoints added.
     *
     * @return The response containing the modified conformance statement.
     */
    @GetMapping("/fhir/metadata")
    public Mono<ResponseEntity<byte[]>> fhirMetadata() {
        debug(() -> "Returning cached metadata.");
        return conformance;
    }

    /**
     * Proxy for all other FHIR requests.  These are a simple pass-through.
     *
     * @param proxy The proxy helper.
     * @return Response from the FHIR server.
     */
    @RequestMapping("/fhir/**")
    public Mono<ResponseEntity<byte[]>> fhirAll(ProxyHelper<byte[]> proxy) {
        debug(() -> "Forwarding FHIR request.");
        return proxy.trimPath("/fhir").forward(fhirEndpoint);
    }

    /**
     * Endpoint for retrieving resources from the gateway server.
     *
     * @param proxy The proxy helper.
     * @return The requested resource.
     */
    @GetMapping("/smart/**")
    public Resource asset(ProxyHelper<byte[]> proxy) {
        Resource resource = new ClassPathResource(proxy.getPath());
        debug(() -> "Retrieving static resource: " + resource);
        _assert(resource.exists(), HttpStatus.NOT_FOUND, () -> "Resource not found: " + resource);
        return resource;
    }

    private void _assert(
            boolean condition,
            HttpStatus status,
            Supplier<String> message) {
        if (!condition) {
            throw new ResponseStatusException(status, message.get());
        }
    }

    private void debug(Supplier<String> message) {
        if (log.isDebugEnabled()) {
            log.debug(message.get());
        }
    }

    private Map<String, String> mapContext(Map<String, String> context) {
        if (!contextMappings.isEmpty()) {
            for (String key : new HashSet<>(context.keySet())) {
                if (contextMappings.containsKey(key)) {
                    String newKey = contextMappings.get(key);
                    String value = context.remove(key);

                    if (newKey != null && !newKey.isEmpty()) {
                        context.put(newKey, value);
                    }
                }
            }
        }

        return context;
    }

}
