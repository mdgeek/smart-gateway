/*
 * #%L
 * SMART Gateway
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * #L%
 */
package edu.utah.kmm.smartgateway;

import org.springframework.core.MethodParameter;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;

/**
 * Argument resolver for proxy helper.
 */
public class ProxyHelperArgumentResolver implements HandlerMethodArgumentResolver {

    private final WebClient webClient;

    public ProxyHelperArgumentResolver(WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return ProxyHelper.class.isAssignableFrom(parameter.getParameterType());
    }

    private Type type(MethodParameter parameter) {
        Type type = parameter.getGenericParameterType();

        if (type instanceof ParameterizedType) {
            ParameterizedType param = (ParameterizedType) type;
            type = param.getActualTypeArguments()[0];
        }

        if (type instanceof TypeVariable || type instanceof WildcardType) {
            type = Object.class;
        }
        return type;
    }

    @Override
    public Mono<Object> resolveArgument(
            MethodParameter parameter,
            BindingContext bindingContext,
            ServerWebExchange exchange) {
        ProxyHelper<?> proxy = new ProxyHelper<>(webClient, exchange, type(parameter));
        return Mono.just(proxy);
    }

}
