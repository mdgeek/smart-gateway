/*
 * #%L
 * SMART Gateway
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * #L%
 */
package edu.utah.kmm.smartgateway;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.springframework.web.reactive.function.client.WebClient.RequestBodyUriSpec;
import static org.springframework.web.reactive.function.client.WebClient.RequestHeadersUriSpec;


/**
 * May be injected as a parameter to @RequestMapping-annotated methods for managing proxy operations.
 *
 * @param <T> The body datatype.
 */
public class ProxyHelper<T> {

    private final WebClient client;

    private final ParameterizedTypeReference<T> type;

    private final HttpHeaders headers;

    private final MultiValueMap<String, String> queryParams;

    private final HttpMethod method;

    private Mono<byte[]> body;

    private String requestPath;

    /**
     * Constructor to be used by {@link ProxyHelperArgumentResolver} class.
     *
     * @param client   The web client.
     * @param exchange Defines the contract for the request/response.
     * @param type     The expected return type of the response body.
     */
    public ProxyHelper(
            WebClient client,
            ServerWebExchange exchange,
            Type type) {
        this.client = client;
        this.type = ParameterizedTypeReference.forType(type);
        ServerHttpRequest request = exchange.getRequest();
        method = request.getMethod();
        requestPath = request.getPath().pathWithinApplication().value();
        queryParams = new LinkedMultiValueMap<>(request.getQueryParams());
        headers = new HttpHeaders(request.getHeaders());
        body = Mono.from(request.getBody().map(data -> data.toString(StandardCharsets.UTF_8).getBytes())).cache();
    }

    /**
     * Constructor for creating an instance on-the-fly.  Since no HTTP method is pre-defined, the {@link #forward}
     * method may not be used.
     *
     * @param client The web client.
     * @param type   The expected return type of the response body.
     */
    public ProxyHelper(
            WebClient client,
            Type type) {
        this.client = client;
        this.type = ParameterizedTypeReference.forType(type);
        this.method = null;
        this.queryParams = new LinkedMultiValueMap<>();
        this.headers = new HttpHeaders();
    }

    /**
     * @return A mutable map of the current query parameters.
     */
    public MultiValueMap<String, String> getQueryParams() {
        return queryParams;
    }

    /**
     * Sets a query parameter value(s).
     *
     * @param name   The query parameter name.
     * @param values The value(s) to set.  Existing values will be replaced.
     * @return The proxy helper instance (for chaining).
     */
    public ProxyHelper<T> setQueryParam(
            String name,
            String... values) {
        queryParams.put(name, Arrays.asList(values));
        return this;
    }

    /**
     * @return The full request path including any query parameters.
     */
    public String getFullPath() {
        return requestPath + encodeQueryParams();
    }

    /**
     * @return The request path excluding any query parameters.
     */
    public String getPath() {
        return requestPath;
    }

    /**
     * Sets the request path.
     *
     * @param path The request path.
     * @return The proxy helper instance (for chaining).
     */
    public ProxyHelper<T> setPath(String path) {
        Assert.notNull(path, "Path must not be null.");
        Assert.isTrue(!path.contains("?"), "The path may not include query parameters");
        Assert.isTrue(!path.startsWith("http"), "The path should be relative.");
        requestPath = path;
        return this;
    }

    /**
     * Trims the specified prefix from the beginning of the current request path.
     *
     * @param prefix The prefix to be trimmed.
     * @return The proxy helper instance (for chaining).
     * @throws IllegalArgumentException if request path does not start with the specified prefix.
     */
    public ProxyHelper<T> trimPath(String prefix) {
        Assert.isTrue(prefix == null || requestPath.startsWith(prefix), "Path does not start with specified prefix.");
        requestPath = prefix == null ? requestPath : requestPath.substring(prefix.length());
        return this;
    }

    /**
     * @return A mutable map of current request headers.
     */
    public HttpHeaders getHeaders() {
        return headers;
    }

    /**
     * Sets the value(s) for a request header.  Any existing values are replaced.
     *
     * @param name   The header name.
     * @param values The header value(s).
     * @return The proxy helper instance (for chaining).
     */
    public ProxyHelper<T> setHeader(
            String name,
            String... values) {
        headers.put(name, Arrays.asList(values));
        return this;
    }

    /**
     * @return The request body.
     */
    public Mono<byte[]> getBody() {
        return body;
    }

    /**
     * Sets the request body.
     *
     * @param body The new request body.
     * @return The proxy helper instance (for chaining).
     */
    public ProxyHelper<T> setBody(byte[] body) {
        this.body = Mono.just(body);
        return this;
    }

    /**
     * @return A map of form data extracted from the body.
     */
    public Mono<MultiValueMap<String, String>> getBodyAsFormData() {
        return body.map(data -> UriComponentsBuilder.newInstance().query(new String(data)).build(false).getQueryParams());
    }

    /**
     * Executes an HTTP DELETE operation.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> delete(String endpoint) {
        return execute(endpoint, client.delete());
    }

    /**
     * Executes an HTTP GET operation.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> get(String endpoint) {
        return execute(endpoint, client.get());
    }

    /**
     * Executes an HTTP HEAD operation.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> head(String endpoint) {
        return execute(endpoint, client.head());
    }

    /**
     * Executes an HTTP OPTIONS operation.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> options(String endpoint) {
        return execute(endpoint, client.options());
    }

    /**
     * Executes an HTTP PATCH operation.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> patch(String endpoint) {
        return execute(endpoint, client.patch());
    }

    /**
     * Executes an HTTP POST operation.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> post(String endpoint) {
        return execute(endpoint, client.post());
    }

    /**
     * Executes an HTTP PUT operation.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> put(String endpoint) {
        return execute(endpoint, client.put());
    }

    /**
     * Executes an HTTP operation based on the original request's method.
     *
     * @param endpoint The target endpoint for the operation.
     * @return The result of the operation.
     */
    public Mono<ResponseEntity<T>> forward(String endpoint) {
        Assert.notNull(method, "Cannot forward without specifying an HTTP method.");

        switch (method) {
            case DELETE:
                return delete(endpoint);

            case GET:
                return get(endpoint);

            case HEAD:
                return head(endpoint);

            case OPTIONS:
                return options(endpoint);

            case PATCH:
                return patch(endpoint);

            case POST:
                return post(endpoint);

            case PUT:
                return put(endpoint);

            default:
                throw new IllegalArgumentException("Bad HTTP method: " + method);
        }
    }

    private Mono<ResponseEntity<T>> execute(
            String endpoint,
            RequestHeadersUriSpec<?> spec) {
        Assert.notNull(endpoint, "Endpoint must not be null.");

        return spec
                .uri(concatPath(endpoint, getFullPath()))
                .headers(h -> h.putAll(this.headers))
                .retrieve()
                .onStatus(status -> status.isError(), resp -> Mono.empty())
                .toEntity(type);
    }

    private Mono<ResponseEntity<T>> execute(
            String endpoint,
            RequestBodyUriSpec spec) {
        spec.body(BodyInserters.fromPublisher(body, byte[].class));
        return execute(endpoint, (RequestHeadersUriSpec<?>) spec);
    }

    private String encodeQueryParams() {
        return queryParams.isEmpty() ? "" : "?" + UriComponentsBuilder.newInstance().queryParams(queryParams).build().getQuery();
    }

    private String concatPath(String... fragments) {
        StringBuilder sb = new StringBuilder();
        String dlm = "";

        for (String fragment : fragments) {
            dlm = fragment.startsWith("/") ? "" : dlm;
            sb.append(dlm).append(fragment);
            dlm = fragment.endsWith("/") ? "" : "/";
        }

        return sb.toString();
    }

}
