/*
 * #%L
 * SMART Gateway
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * #L%
 */
const href = window.location.href;
const i = href.indexOf('/smart');
const base = href.substring(0, i);
const messageTemplate = 'data:text/html;charset=utf-8,<h1 style="color:@;text-align:center">** @ **</h1>';
const iframe = document.getElementById('iframe');
const header = document.getElementById('header');

let launchUrl;
let appName = 'SMART Gateway';
start();

function start() {
    const params = fromQueryString();
    const target = getParam(params, 'target');

    if (!target) {
        failure('No target was specified.');
        return;
    }

    if (getParam(params, 'noheader') !== undefined) {
        header.style.display = 'none';
    }

    get(base + '/target?target=' + target, resp =>
        init(params, resp), failure);
}

function getParam(params, param) {
    const result = params[param];

    if (result !== undefined) {
        delete params[param];
    }

    return result;
}

function init(params, resp) {
    const i = resp.indexOf('|');

    if (i > 0) {
        launchUrl = resp.substring(0, i);
        appName = resp.substring(i + 1) || appName;
    } else {
        launchUrl = resp;
    }

    document.title = appName;

    if (params.patient) {
        refresh(params);
    }

    if (typeof CCOW !== 'undefined') {
        CCOW.initialize(appName, '127.0.0.1:2116', refresh);
    } else if (params.patient) {
        console.log('No CCOW context manager found.')
    } else {
        failure('No patient specified.');
    }
}

function refresh(context) {
    message(appName + ' is initializing...');
    const patientName = getParam(context, 'patient.co.patientname');

    if (patientName) {
        const pcs = patientName.replace(/\+/g, ' ').split('^');
        setHeader((pcs[0] || '') + (pcs[1] ? ', ' + pcs[1] : ''));
    } else {
        setHeader();
    }

    get(base + '/launch' + toQueryString(context), launch, failure);
}

function launch(launchId) {
    const tgtParams = {
        launch: launchId,
        iss: base + '/fhir'
    }
    const pcs = launchUrl.split('?', 2);
    iframe.src = pcs[0] + toQueryString(tgtParams, pcs[1]);
}

function setHeader(text) {
    while (header.firstChild) {
        header.removeChild(header.firstChild);
    }

    if (text) {
        header.appendChild(document.createTextNode(text));
    }
}

function failure(errorText) {
    message(errorText || 'An error occurred during application launch.', true);
}

function message(message, isError) {
    setHeader();
    let html = messageTemplate;
    html = html.replace('@', isError ? 'red' : 'black');
    html = html.replace('@', message);
    iframe.src = html;
}

// Create query string from parameter map.

function toQueryString(params, qs) {
    qs = qs || '';

    let dlm = qs ? '&' : '?';

    if (params) {
        for (let param in params) {
            qs += dlm + param + '=' + encodeURIComponent(params[param]);
            dlm = '&';
        }
    }

    return qs[0] === '?' ? qs : ('?' + qs);
}

// Parse query string into parameter map.

function fromQueryString(qs) {
    qs = qs || window.location.search.substring(1);

    const
        params = qs.split('&'),
        map = {};

    for (let i = 0; i < params.length; i++) {
        const keyval = params[i].split('='),
            key = keyval[0],
            val = keyval[1];

        map[key] = decodeURIComponent(val);
    }

    return map;
}

// Performs an HTTP GET operation

function get(url, success, failure) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            xhr.status === 200 ? success(xhr.responseText) : failure(xhr.responseText);
        }
    }
    xhr.open('GET', url);
    xhr.send();
}
