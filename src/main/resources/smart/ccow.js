/*
    Simple CCOW Web Participant.  Uses polling to detect context changes.
 */
const CCOW = {
    appName: null,
    contextManager: null,
    contextParticipant: null,
    onUpdateContext: null,
    participantCoupon: 0,
    contextCoupon: 0,
    site: null,
    intervalTimer: null,

    initialize: function (appName, contextParticipant, onUpdateContext) {
        window.addEventListener('beforeunload', () => CCOW.leave());
        CCOW.appName = appName;
        CCOW.contextParticipant = contextParticipant;
        CCOW.onUpdateContext = onUpdateContext;
        CCOW.invoke('http://localhost:2116',
            {
                interface: 'ContextManagementRegistry',
                method: 'Locate',
                componentName: 'CCOW.ContextManager',
                version: '1.5',
                contextParticipant: CCOW.contextParticipant
            },
            response => {
                console.log('CCOW init: ' + response);
                CCOW.site = response.site;
                CCOW.contextManager = response.componentUrl;
                CCOW.join();
            });
    },

    join: function(callback) {
        if (!CCOW.participantCoupon) {
            CCOW.cmInvoke({
                method: 'JoinCommonContext',
                applicationName: CCOW.appName,
                contextParticipant: CCOW.contextParticipant,
                survey: false,
                wait: false
            }, response => {
                CCOW.participantCoupon = parseInt(response.participantCoupon);
                CCOW.poll();
                CCOW.startPolling();
            }, callback);
        }
    },

    leave: function (callback) {
        if (CCOW.participantCoupon) {
            CCOW.stopPolling();
            const participantCoupon = CCOW.participantCoupon;
            CCOW.participantCoupon = null;

            CCOW.cmInvoke({
                method: 'LeaveCommonContext',
                participantCoupon: participantCoupon
            }, callback);
        }
    },

    refreshContext: function () {
        CCOW.cdInvoke({
            method: 'GetItemValues',
            itemNames: '*',
            onlyChanges: false,
            contextCoupon: CCOW.contextCoupon
        }, response => {
            const ary = response.values.split('|');
            const values = {};
            let item;

            for (let i = 0; i < ary.length; i+=2) {
                item = ary[i + 1];

                if (item) {
                    values[ary[i]] = item;
                }
            }

            CCOW.onUpdateContext(values);
        });
    },

    cmInvoke: function (params, cb) {
        params.interface = 'ContextManager';
        CCOW.invoke(CCOW.contextManager, params, cb);
    },

    cdInvoke: function (params, cb) {
        params.interface = 'ContextData';
        CCOW.invoke(CCOW.contextManager, params, cb);
    },

    invoke: function (url, params, cb) {
        let stopOnError;

        if (params.stopOnError) {
            delete params.stopOnError;
            stopOnError = true;
        }

        get(url + toQueryString(params),
            response => cb ? cb(fromQueryString(response)) : null,
            error => {
                const message = 'Error invoking CCOW service ' + params.interface + '::' + params.method + ': ' + error;

                if (stopOnError) {
                    failure(message);
                    CCOW.leave();
                } else {
                    console.error(message);
                }
            });
    },

    startPolling: function () {
        CCOW.stopPolling();
        CCOW.intervalTimer = setInterval(CCOW.poll, 2000)
    },

    stopPolling: function () {
        if (CCOW.intervalTimer) {
            clearInterval(CCOW.intervalTimer);
            CCOW.intervalTimer = null;
        }
    },

    poll: function () {
        CCOW.cmInvoke({
            method: 'GetMostRecentContextCoupon',
            stopOnError: true
        }, response => {
            const contextCoupon = parseInt(response.contextCoupon);

            if (contextCoupon !== CCOW.contextCoupon) {
                CCOW.contextCoupon = contextCoupon;
                CCOW.refreshContext();
            }
        });
    }
}
